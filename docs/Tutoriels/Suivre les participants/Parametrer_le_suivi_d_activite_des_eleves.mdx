---
sidebar_position: 1
tags:
  - activité
  - Suivi des élèves
  - Paramétrer
---
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';


# Paramétrer le suivi d'activité des élèves

Ce tutoriel (le texte et la vidéo d'accompagnement) a pour objectif de vous présenter **les outils de suivi** disponibles sur la plateforme Éléa, et de vous expliquer **comment les paramétrer**.

<details>
<summary> Tutoriel vidéo</summary>
<iframe title="Suivre la progression des élèves > Paramétrer le suivi d'activité des élèves" src="https://podeduc.apps.education.fr/video/66289-suivre-la-progression-des-eleves-parametrer-le-suivi-dactivite-des-eleves/?is_iframe=true" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="640" height="360" frameborder="0"></iframe>
</details>

Pour suivre facilement la progression de vos élèves sur un parcours, vous vous placerez sur votre page d'accueil (Mes parcours), et vous cliquerez sur l'icône de Suivi ![L'icône de Suivi](img/parametrer_suivi/capture1.png), à droite du nom du parcours ; vous arrivez alors sur une page nommée **Suivi du parcours "Nom du parcours"**.

## Comprendre le tableau de suivi ##

![Le tableau de suivi](img/parametrer_suivi/capture2.png#printscreen#centrer)

- Colonne de gauche, la liste des élèves inscrits à ce parcours.
- Colonnes suivantes, il s'agit de chacune des activités suivies, avec indication d'achèvement :

Les cases restent blanches tant que l'élève n'a pas atteint l'activité, elles deviennent bleues ensuite lorsque l'activité ou la ressource est considérée comme achevée (ce qui ne veut pas dire qu'elle est forcément réussie).

La présence d'un cercle sur des cases signale des activités avec une note de type QCM, Appariement ou Millionnaire : lorsque l'élève a réussi l'activité, le cercle devient noir. Le nombre dans le cercle renseigne sur le nombre de tentatives au total.

On remarque que ce ne sont pas seulement les activités évaluées qui sont reprises dans ce tableau de suivi : Éléa sait suivre une simple consultation de ressource, par exemple (dans l'exemple ci-dessus, ce sont des ressources Pages qui sont suivies). Il suffit que l'activité ou la ressource ait une condition d'achèvement pour figurer dans ce tableau de suivi.

:::info
Pour obtenir un **suivi plus précis**, avec des **notes** par exemple, il faut **entrer dans le parcours**.
:::


## Faire le lien avec les notes
Pour afficher le tableau de notes, vous entrez dans le parcours, puis vous cliquez sur l'onglet Notes du menu du parcours.

![Les onglets du menu du cours](img/parametrer_suivi/capture3.png#printscreen#centrer)

Vous arrivez sur une page intitulée Rapport de l'évaluateur : vous retrouvez la liste de vos élèves inscrits au parcours, les différentes évaluations qui se trouvent dans le parcours, et les résultats des élèves.

![Le tableau de notes](img/parametrer_suivi/capture4.png#printscreen#centrer)


## Paramétrer ses outils de suivi

On distingue deux types d'activités sur Éléa :
- les activités proposant des **ressources** (ce sont la plupart du temps des **Pages**, ou une ressource **Fichier**) ;
- les activités donnant lieu à une **évaluation** ; c'est le cas des activités **Appariement**, **QCM**, **Millionnaire**, c'est le cas des **Tests**, ou encore des **Devoirs**.

### Paramétrer les activités proposant simplement de la ressource

Pour paramétrer les activités ne proposant que de la ressource, il suffit d'entrer dans le paramétrage de ces activités.
On passe en mode édition (le bouton en haut à droite), et on entre dans les paramètres de l'activité à configurer en passant par ce menu de l'activité :

![Afficher les paramètres](img/parametrer_suivi/capture5.png#printscreen#centrer)

On se rend en bas de la page de paramétrage, à l'onglet **Achèvement d'activité**.
Ici, 3 options : 

![3 possibilités de suivi](img/parametrer_suivi/capture6.png#printscreen#centrer)

- **Ne pas afficher l'état d'achèvement** : ce choix, qui convient dans certains cas, empêchera le suivi de vos élèves.
- **Les participants peuvent marquer manuellement cette activité comme terminée** : vous demandez à vos élèves de cocher une case pour indiquer qu'ils ont terminé l'activité. Pour marquer cette activité, il faut qu'ils se rendent dans la section contenant la ressource pour trouver le bouton (le thème d'Éléa ne permet pas pour l'instant de marquer l'achèvement directement dans la page). 

![Marquer la page](img/parametrer_suivi/capture10.png#printscreen#centrer)

Mais ils pourraient marquer l'activité sans avoir affiché ou lu la ressource.

- On préfèrera alors **Afficher l'activité comme terminée dès que les conditions sont remplies** : conditions que l'enseignant définit à l'étape suivante.

Pour la Page, qui propose du contenu mais pas de mise en activité, on choisira **L'étudiant doit afficher cette activité pour achever l'activité**. C'est la meilleure option pour suivre la consultation d'informations ou de ressources.

On a la possibilité de définir une contrainte calendaire en rapport avec cet achèvement ; dans ce cas, la date est automatiquement reprise dans le calendrier du cours si vous l'ajoutez colonne de droite.

![Paramétrage du suivi avec affichage de la ressource, et contrainte calendaire](img/parametrer_suivi/capture7.png#printscreen#centrer)

On enregistre et on revient au cours.

### Paramétrer les activités donnant lieu à une évaluation

Même démarche : on passe en **mode édition**, on entre dans les **paramètres** de l'activité, et on se rend sous l'onglet **Achèvement d'activité**.

Avec une activité donnant lieu à une évaluation, on va là aussi pouvoir automatiser le suivi.

Une nouvelle fois, je choisis **Afficher l'activité comme terminée dès que les conditions sont remplies** : cette fois-ci, on ne cochera pas l'**Affichage requis**, mais l'**obtention d'une note** pour terminer l'activité.

On peut paramétrer finement ce que l'on souhaite (obtention d'une note minimale pour que l'activité soit considérée comme achevée, ou simple obtention d'une note), et ajouter, comme dans l'exemple précédent, une contrainte calendaire.

![Paramétrage du suivi avec obtention d'une note](img/parametrer_suivi/capture8.png#printscreen#centrer)

On enregistre et on revient au cours.

## Repérer dans le parcours les conditions d'achèvement

On peut retrouver dans le parcours les conditions d'achèvement paramétrées, sans avoir à entrer dans chacune des activités.

Généralement, au niveau d'une **ressource** on trouvera la condition **Consulter**, et au niveau d'une **activité évaluée**, on trouvera **Recevoir une note**, et **L'étudiant doit réussir l'activité ou la tenter une ou plusieurs fois**. 

Côté élève, on a les mêmes informations, avec le statut (À faire, ou Terminé) : 

![Affichage des conditions d'achèvement](img/parametrer_suivi/capture9.png#printscreen#centrer)
