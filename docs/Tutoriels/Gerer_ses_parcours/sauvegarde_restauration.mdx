---
tags:
  - parcours
  - sauvegarde
  - restauration
  - importation
  - gestion de parcours
---
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Sauvegarde, restauration et importation de parcours

Dans ce tutoriel, vous allez apprendre à effectuer des sauvegardes de vos parcours pour les dupliquer ou les faire migrer d'une plateforme à une autre. Vous apprendrez à restaurer les sauvegardes en totalité ou en fusionnant une partie avec un autre parcours. Vous apprendrez également à importer tout ou partie d'un cours dans un parcours existant pour l'enrichir ou le modifier.

## 1 - Sauvegarder ses parcours

:::tip Quand sauvegarder votre cours ?

* Après une modification importante ou en fin d'année, par sécurité
* Si vous changez d'établissement et de zone géographique (vous pourrez alors **restaurer** votre sauvegarde sur votre nouvelle plateforme)
* Pour tout échange de cours avec des collègues
:::

:::danger 
* La taille du fichier à sauvegarder ne doit pas dépasser **250 Mo**, sinon vous ne pourrez pas le restaurer sur la plateforme Éléa. Si c'est le cas, vous devrez faire une sauvegarde par parties du parcours, puis de les **fusionner** lors de la restauration.

* La sauvegarde ne contient pas les **badges** qui ont été insérés dans le parcours. Il conviendra de les rajouter manuellement lors de sa restauration.
:::

### 1-1 Sauvegarde complète

Dans votre parcours, en mode "**édition**", dépliez dans le "**menu d'édition**" de droite la ligne "**Import/export**". 

![menu d'édition](img/sauvegarde_restauration/Capture0.png#printscreen#centrer)

Cliquez sur "**Exporter le parcours**".

Après quelques secondes, une fenêtre surgissante apparaît vous proposant d'enregistrer votre fichier de sauvegarde dans un dossier de votre ordinateur. L'extension du fichier est au format Moodle en (.mbz).

![fenêtre surgissante](img/sauvegarde_restauration/Capture1.png#printscreen#centrer)

Cliquez ensuite sur "**Continuer**" pour revenir sur votre parcours.

### 1-2 Sauvegarde d'une partie du parcours

Sur votre parcours, en mode "édition", dans la barre de menu située en haut du parcours,

![admin-icon](img/sauvegarde_restauration/capture0a.png#printscreen#centrer)

dépliez le menu "**Plus**", puis cliquez sur "**Réutilisation de cours**".

![admin-icon](img/sauvegarde_restauration/capture1a.png#printscreen#centrer)

Pour finir, cliquez sur Importation, puis sélectionnez "**Sauvegarde**".

![admin-icon](img/sauvegarde_restauration/capture2b.png#printscreen#centrer)

Il s'agit maintenant de choisir les données qui seront sauvegardées. Une fois les éléments inclus, cliquez sur "**Suivant**".

![admin-icon](img/sauvegarde_restauration/capture2a.png#printscreen#centrer)
 
:::tip Remarque

Il n'est pas possible actuellement d'inclure les **badges**. Il conviendra de les rajouter **manuellement** après la restauration.
:::

Il vous est possible de ne sauvegarder qu'une partie du parcours. Pour cela, il vous suffit de décocher certains éléments pour qu'ils ne figurent pas dans la sauvegarde.

![admin-icon](img/sauvegarde_restauration/Capture9.png#printscreen#centrer)

Cliquez sur "**Suivant**" puis "**Effectuer la sauvegarde**" et pour finir sur "**Continuer**".

![admin-icon](img/sauvegarde_restauration/capture05.png#printscreen#centrer)

Dans la "**zone de sauvegarde privée**", vous pouvez télécharger le fichier de sauvegarde sur votre ordinateur. 

![admin-icon](img/sauvegarde_restauration/capture2aa.png#printscreen#centrer)

:::tip Bon à savoir
Votre "**Zone de sauvegarde privée**" est accessible à partir du menu "**restauration**" vu précédement et contient toutes vos sauvegardes effectuées par la méthode "Sauvegarde partielle" décrite ci-dessus. Vous pouvez supprimer, ajouter ou télécharger vos fichiers en cliquant sur le menu "**Gérer les fichiers de sauvegarde**".

![menu d'édition](img/sauvegarde_restauration/Capture11.png#printscreen#centrer)
:::


## 2 - Restaurer un parcours

:::tip Bon à savoir
La restauration vous permet :
* de **dupliquer** votre parcours sur la même plateforme ou faire **migrer** votre parcours sur une autre plateforme : vous allez d'abord créer un cours **VIDE** et vous y restaurerez ensuite le contenu de la sauvegarde.
* d'**enrichir** ou **modifier** un cours existant avec une sauvegarde : Vous **entrez dans ce cours** pour y restaurer votre fichier de sauvegarde.
:::

### 2-1 Dupliquer votre parcours sur la même plateforme ou le migrer sur une autre plateforme

Sur la plateforme où vous souhaitez restaurer le parcours : 

Créer un nouveau parcours en utilisant **impérativement** le gabarit "**parcours vide**" !

![admin-icon](img/sauvegarde_restauration/parcours_vide.png#printscreen#centrer)

Nommez votre parcours en utilisant soit le même nom que votre parcours sauvegardé, soit avec un nom alternatif (choix conseillé).

![admin-icon](img/sauvegarde_restauration/parcours_vide2.png#printscreen#centrer)

Rendez-vous dans ce nouveau parcours.

Cliquez dans le "**menu d'édition**" de droite, sur la ligne "**Import/export**". 

![menu d'édition](img/sauvegarde_restauration/Capture2.png#printscreen#centrer)

Cliquez sur "**Importer un parcours**".

Dans la fenêtre surgissante, cliquez sur "**choisir un fichier**", ou faites glisser votre fichier de sauvegarde dans la zone à cet effet.

![menu d'édition](img/sauvegarde_restauration/Capture3.png#printscreen#centrer)

puis sélectionnez votre fichier de sauvegarde.

![menu d'édition](img/sauvegarde_restauration/Capture4.png#printscreen#centrer)

Cliquez ensuite sur "**Importer**".

S'il n'y a pas d'erreur, un message indiquant que le cours a été correctement importé s'affiche.

![menu d'édition](img/sauvegarde_restauration/Capture5.png#printscreen#centrer)

Cliquez sur "**Continuez**" pour terminer et revenir sur votre parcours.


### 2-2 Enrichir ou modifier un parcours existant à partir d'une sauvegarde

Rendez-vous sur le parcours à enrichir.

Dans **la barre de menu** située en haut du parcours,

![admin-icon](img/sauvegarde_restauration/capture0a.png#printscreen#centrer)

dépliez le menu "**Plus**", puis cliquez sur "**Réutilisation de cours**".

![admin-icon](img/sauvegarde_restauration/capture1a.png#printscreen#centrer)

Pour finir, cliquez sur "**Importation**", puis sélectionnez "Restauration".

![admin-icon](img/sauvegarde_restauration/capture3a.png#printscreen#centrer)

Dans la fenêtre surgissante, cliquez sur "**choisir un fichier**", ou faites glisser votre fichier de sauvegarde dans la zone à cet effet.

![admin-icon](img/sauvegarde_restauration/capture4a.png#printscreen#centrer)

- Cliquez sur le bouton "**Choisir un fichier**" et sélectionnez dans votre ordinateur la sauvegarde que vous venez de faire de votre parcours.

![admin-icon](img/sauvegarde_restauration/capture5a.png#printscreen#centrer)

Il ne vous reste plus qu'à cliquer sur "**Continuer**".

![admin-icon](img/sauvegarde_restauration/capture6a.png#printscreen#centrer)

Cliquez en bas à droite sur "**Continuer**".

Cochez **impérativement** "**Fusionner le cours sauvegardé avec ce cours**". Cliquez sur "**Continuer**".

![admin-icon](img/sauvegarde_restauration/capture7a.png#printscreen#centrer)

Lors de cette phase de la restauration, il vous est possible de :
* renommer votre cours, 
* de définir une date de début du cours
* de conserver les groupes

![admin-icon](img/sauvegarde_restauration/capture8a.png#printscreen#centrer)

* De restaurer qu'une partie du parcours sauvegardé en désélectionnant les activités ou sections à ne pas restaurer.

![admin-icon](img/sauvegarde_restauration/capture9a.png#printscreen#centrer)

Cliquez sur "**Effectuer la restauration**" puis sur "**Continuer**" à 2 reprises.

![admin-icon](img/sauvegarde_restauration/capture10a.png#printscreen#centrer)

![admin-icon](img/sauvegarde_restauration/capture11a.png#printscreen#centrer)

:::danger Attention

Si le parcours modèle comprend des badges, il convient de les [ajouter](https://moodle-doc.forge.apps.education.fr/elea/docs/Tutoriels/Gerer_ses_parcours/Ajouter_un_badge "tutoriel pour ajouter un badge") manuellement.
:::

## 3 - Importer un parcours 

:::tip Bon à savoir
Importer un parcours consiste à fusionner tout ou partie d'un parcours (sans en faire la sauvegarde) dans un parcours existant sur **la même plateforme**, pour l'enrichir ou modifier des parties.

L'importation à partir d'un parcours vide peut être utilisée pour dupliquer facilement un parcours sans en faire la sauvegarde.
:::

Rendez-vous sur le parcours à enrichir.

Dans la barre de menu située en haut du parcours,

![admin-icon](img/sauvegarde_restauration/capture0a.png#printscreen#centrer)

dépliez le menu "**Plus**", puis cliquez sur "**Réutilisation de cours**".

![admin-icon](img/sauvegarde_restauration/capture1a.png#printscreen#centrer)

Pour finir, cliquez sur "**Importation**", 

![admin-icon](img/sauvegarde_restauration/Capture6.png#printscreen#centrer)

Choisissez le parcours à importer dans la liste en **cochant le bouton** de la première colonne. Utilisez le moteur de recherche en bas pour rechercher un parcours si nécessaire.

![admin-icon](img/sauvegarde_restauration/Capture7.png#printscreen#centrer)

Puis, cliquez sur "**Continuer**".

Cliquez sur "**Passer à la dernière étape**" si vous souhaitez importer la totalité du parcours, sinon, cliquez sur "**Suivant**".

![admin-icon](img/sauvegarde_restauration/Capture8.png#printscreen#centrer)

Dans ce cas, choisissez les sections et activités à conserver et décochez celles que vous ne voulez pas importer.

![admin-icon](img/sauvegarde_restauration/Capture9.png#printscreen#centrer)

Cliquez sur "**Suivant**", et cliquez sur "**Effectuer l'importation**".

S'il n'y a pas d'erreur, un message indiquant que le cours a été correctement importé s'affiche.

![menu d'édition](img/sauvegarde_restauration/Capture10.png#printscreen#centrer)

Cliquez sur "**Continuez**" pour terminer et revenir sur votre parcours.
