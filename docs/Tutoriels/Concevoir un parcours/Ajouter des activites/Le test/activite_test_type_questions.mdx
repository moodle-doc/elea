---
sidebar_position: 27
tags:
  - activité
  - Test
  - quiz
  - évaluer
---
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Les différents types de questions

**L'activité Test permet de concevoir des tests auto-corrigés**, composés de questions variées (choix
unique ou choix multiples, appariement, vrai-faux, réponse courte, réponse numérique, glisser-déposer...)

Nous allons détailler ici les différents types de questions qu'il est possible d'intégrer.
Vous pouvez utiliser le sommaire à droite de cet écran pour vous rendre directement sur le type de question voulu.

## QCM/QCU

![QCM](img/activite_test/qtype2.png#printscreen#centrer)

1. Donnez un nom à la question dans le champ "**Nom de question**". Il apparaîtra dans la page de modification du test.
2. Rédigez l'énoncé de la question dans le champ "**Texte de la question**". C'est la question telle que la verront les élèves. Vous pouvez insérer une image, une vidéo à l'aide de l'éditeur habituel…
3. Indiquez la note par défaut (la note maximale pour cette question).
4. Il est possible de compléter un **feedback général** : cette rétroaction s'affichera une fois les réponses de l'élève envoyées.
5. Facultatif : Ajouter un numéro d'identification pour retrouver facilement la question dans les catégories de la banque de questions. Il doit être unique dans chaque catégories de la banque de questions. Laissez vide la plupart du temps.
6. Faites le choix d'**une seule** ou **plusieurs réponses** correctes dans le menu déroulant.
7. Cochez (ou non) la case **Mélanger les réponses possibles**.
8. Choisissez le type de numérotation des questions.

#### Réponses :
9. Inscrivez, à **Réponse 1**, le contenu de la réponse.
10. Sélectionnez à **Note**, la note à cette réponse
    - **Choix unique** : 100% pour la bonne réponse / Aucun pour les autres propositions.
    - **Choix Multiples** : Le total des bonnes réponses doit faire 100 % (ex. Si 3 bonnes réponses, chacune vaut 33,3%)
11. Inscrivez dans **Feedback** le commentaire formatif à cette réponse (Optionnel). Ces rétroactions s'afficheront seulement si, dans les options du test, l'option « **Afficher la rétroaction** » est activée.

#### Complétez autant de réponses que nécessaire.

:::info **Option : Tentatives multiples**
- Sélectionnez la pénalité lorsque des tentatives sont incorrectes.
- Complétez le champ "indice 1". L'élève qui se trompe pourra recommencer la question avec cet indice.
- Dans les paramètres du Test, il faudra choisir "**Interactif avec essais multiples**" dans la rubrique "**Comportement des questions**".
:::

12. Remplissez les autres choix de réponse du formulaire. Tout champ vide sera ignoré.
13. Cliquez sur le bouton "**Enregistrer**" une fois terminé.

:::info Facteur de pénalité

Le facteur de pénalité s'applique seulement lorsque la question est utilisée dans un test en mode adaptatif, c'est-à-dire où l'étudiant a droit à plusieurs tentatives pour une même question au cours d'une même tentative au test. Le facteur de pénalité est un pourcentage que l'étudiant perd sur la note maximale de la question à chaque tentative autre que la première. Par exemple, si la note par défaut est de 10, et que le facteur de pénalité est de 25 %, alors chaque tentative à la question autre que la première fera perdre à l'étudiant 2,5 points. 
:::

## Question vrai-faux

![vrai-faux](img/activite_test/qtype7.png#printscreen#centrer)

1. Donnez un nom descriptif à la question dans le champ "**Nom de question**". Il apparaîtra dans la page de modification du test et permettra de la retrouver facilement dans la banque de questions.
2. Rédigez l'énoncé de la question dans le champ "**Texte de la question**",. C'est la question telle que la verront les élèves. Vous pouvez insérer une image, une vidéo à l'aide de l'éditeur habituel…
3. Indiquez la note par défaut (la note maximale pour cette question).
4. Il est possible de compléter un **feedback général** : cette rétroaction s'affichera une fois les réponses de l'élève envoyées.
5. Facultatif : Ajouter un numéro d'identification pour retrouver facilement la question dans les catégories de la banque de questions. Il doit être unique dans chaque catégories de la banque de questions. Laissez vide la plupart du temps.
6. Sélectionnez la bonne réponse - vrai ou faux.
7. Saisissez une rétroaction pour les réponses vrai et faux.
8. Cliquez sur le bouton "**Enregistrer**" une fois terminé.

:::info Facteur de pénalité

Le facteur de pénalité s'applique seulement lorsque la question est utilisée dans un test en mode adaptatif, c'est-à-dire où l'étudiant a droit à plusieurs tentatives pour une même question au cours d'une même tentative au test. Le facteur de pénalité est un pourcentage que l'étudiant perd sur la note maximale de la question à chaque tentative autre que la première. Par exemple, si la note par défaut est de 10, et que le facteur de pénalité est de 25 %, alors chaque tentative à la question autre que la première fera perdre à l'étudiant 2,5 points. 
:::

## L'appariement

![appariement](img/activite_test/qtype3.png#printscreen#centrer)

La question "**Appariement**" permet d'associer différents couples : Texte-Texte, Image-Texte, Média(Son, vidéo)-Texte.
1. Inscrivez, dans le champ **Nom de question**, le nom de la question. Cela vous permettra de la retrouver facilement dans la banque de questions.
2. Rédigez, dans le champ **Texte de la question**, l'énoncé de la question tel qu'il apparaîtra aux élèves.
3. Indiquez la note par défaut (la note maximale pour cette question).
4. Il est possible de compléter un **feedback général** : cette rétroaction s'affichera une fois les réponses de l'élève envoyées.
5. Vous pouvez **mélanger** les différentes propositions ou les laisser toujours dans le même ordre.
6. Complétez chacun des couples :
    - Question : un texte, une image, une vidéo, un son
    - Réponse : obligatoirement un texte
7. Vous pouvez paramétrer un feedback combiné pour toutes réponses correctes, ou pour toute réponse partiellement correcte.
8. Vous pouvez également paramétrer des pénalités en cas de tentatives multiples.
9. Terminez votre question en cliquant sur "**Enregistrer**"

## Réponse courte

![appariement](img/activite_test/qtype8.png#printscreen#centrer)

1. Inscrivez, dans le champ **Nom de question**, le nom de la question. Cela vous permettra de la retrouver facilement dans la banque de questions.
2. Rédigez, dans le champ **Texte de la question**, l'énoncé de la question tel qu'il apparaîtra aux élèves.
3. Indiquez la note par défaut (la note maximale pour cette question).
4. Il est possible de compléter un **feedback général** : cette rétroaction s'affichera une fois les réponses de l'élève envoyées.
5. Facultatif : Ajouter un numéro d'identification pour retrouver facilement la question dans les catégories de la banque de questions. Il doit être unique dans chaque catégories de la banque de questions. Laisser vide la plupart du temps.
6. Choisissez si les réponses sont sensibles à la casse (minuscule ou majuscule). Activez l'option de sensibilité à la casse si les majuscules sont importantes dans la réponse.
7. Saisissez les réponses acceptables. Vous pouvez décider de donner une partie des points si la réponse est bonne, mais contient des erreurs d'orthographe.
8. Attribuez une note pour chaque réponse.
9. Saisissez de la rétroaction pour chaque réponse.
:::tip Note
Vous pouvez utiliser l'astérisque (*) comme joker pour indiquer que n'importe quelle chaine de caractères est accepté à cet endroit. Par exemple, écrivez pla*ement pour accepter tout mot ou toute proposition commençant par pla et se terminant par ement. Si vous voulez que la réponse contienne vraiment un astérisque, utilisez la barre oblique comme ceci : \*
Il est recommandé d'utiliser le **joker ** pour la dernière réponse et de saisir une rétroaction et une note pour toute autre réponse donnée par les élèves.
:::
10. Vous pouvez également paramétrer des pénalités en cas de tentatives multiples.
11. Terminez votre question en cliquant sur "**Enregistrer**".

## Numérique

![Numérique](img/activite_test/qtype6.png#printscreen#centrer)

Cette question permet une réponse numérique, le cas échéant avec des unités, qui est évaluée en comparant divers modèles de réponses, comprenant une tolérance.
Du point de vue de l'élève, une question à réponse numérique ressemble à une question à réponse courte.

1. Complétez le Nom de la question
2. Rédigez, à Texte de la question, l'énoncé de la question tel qu'il apparaîtra aux élèves.

:::tip **À savoir** :  
Vous pouvez insérer une équation, grâce à l'éditeur Wiris de Moodle qui vous permettent un affichage adéquat des équations.
:::

#### réponses

3. Saisissez la première réponse acceptée.
:::tip **À savoir** : 
- Pour un **nombre décimal**, il faut utiliser un point
- Pour les **puissances**, vous pouvez utiliser 10^ou E
:::

4. **Saisissez la marge d'erreur** pour cette réponse. Il s'agit de la plage de réponses acceptables autour de la réponse juste. Par exemple, si la réponse correcte est 5, les réponses 4 et 6 seront acceptées si la marge d'erreur est de 1.
5. Saisissez une **note** pour cette réponse.
6. Vous pouvez saisir une **rétroaction (feedback)**

**Répétez ces 4 dernières étapes pour chaque réponse que vous souhaitez accepter comme correcte.**

:::tip **À savoir** :  
Vous pouvez utiliser **le joker (\*)** pour toutes les autres réponses (non acceptables) et saisir une **rétroaction qui s'appliquera à toute mauvaise réponse**, ainsi que la **note Aucune**.
:::

## Composition

![Composition](img/activite_test/qtype9.png#printscreen#centrer)

1. Inscrivez, dans le champ **Nom de question**, le nom de la question. Cela vous permettra de la retrouver facilement dans la banque de questions.
2. Rédigez, dans le champ **Texte de la question**, l'énoncé de la question tel qu'il apparaîtra aux élèves.
3. Indiquez la note par défaut (la note maximale pour cette question).
4. Il est possible de compléter un **feedback général** : cette rétroaction s'affichera une fois les réponses de l'élève envoyées.
5. Facultatif : Ajouter un numéro d'identification pour retrouver facilement la question dans les catégories de la banque de questions. Il doit être unique dans chaque catégories de la banque de questions. Laissez vide la plupart du temps.
6. Indiquez dans les options de réponse quel type d'éditeur l'élève pourra utiliser et le nombre de lignes ou de mots attendus pour la réponse.
7. Vous pouvez fournir un modèle de réponse si nécessaire.
8. Terminez votre question en cliquant sur "**Enregistrer**".

## Question CLOZE

La question Cloze est une question qui utilise un langage de programmation spécifique. Son avantage est de permettre par une entrée unique la création de questions variées.

![cloze](img/activite_test/qtype1.png#printscreen#centrer)

1. Inscrivez, dans le champ "**Nom de question**", le nom de la question. Cela vous permettra de la retrouver facilement dans la banque de questions.
2. Rédigez, dans le champ "**Texte de la question**", l'énoncé de la question. 
Vous pouvez commencer à rédiger votre texte et c'est ici qu'on insèrera les réponses possibles aux questions.
Pour insérer les réponses possibles, vous pouvez utiliser l'éditeur de questions cloze disponible dans les boutons de l'éditeur de texte :
- Dépliez le menu en cliquant sur le premier bouton.
- Cliquez sur le bouton correspondant à l'éditeur de questions : 

![cloze](img/activite_test/qtype10.png#printscreen#centrer)

Dans la fenêtre surgissante, choisissez le type de question à ajouter (format des réponses) et cliquez sur le bouton **"Ajouter"**.

![cloze](img/activite_test/qtype11.png#printscreen#centrer)

Vous trouverez ci-dessous un résumé des différents choix possibles.

<details>
<summary> Description des types de questions </summary>

- MULTICHOICE: ou son abréviation :MC: -> Question à choix multiple est présentée dans un menu déroulant. L'ordre des choix de réponses est fixe.
- MULTICHOICE_H: ou son abréviation :MCH: -> Question à choix multiple, les réponses sont alignées horizontalement et l'ordre est fixe.
- MULTICHOICE_V: ou son abréviation :MCV: -> Question à choix multiple, les réponses sont alignées verticalement et l'ordre est fixe.
- MULTICHOICE_S: ou son abréviation :MCS: -> Question à choix multiple est présentée dans un menu déroulant. L'ordre des choix de réponses est mélangé.
- MULTICHOICE_HS: ou son abréviation :MCHS: -> Question à choix multiple, les réponses sont alignées horizontalement et l'ordre est mélangé.
- MULTICHOICE_VS: ou son abréviation :MCVS: -> Question à choix multiple, les réponses sont alignées verticalement et l'ordre est mélangé.
- MULTIRESPONSE: ou son abréviation :MR: -> Question à choix multiple est présentée dans un menu déroulant. L'ordre des choix de réponses est fixe.
- MULTIRESPONSE_H: ou son abréviation :MRH: -> Question à choix multiple, les réponses sont alignées horizontalement et l'ordre est fixe.
- MULTIRESPONSE_S: ou son abréviation :MRS: -> Question à choix multiple est présentée dans un menu déroulant. L'ordre des choix de réponses est mélangé.
- MULTIRESPONSE_HS: ou son abréviation :MRHS: -> Question à choix multiple, les réponses sont alignées horizontalement et l'ordre est mélangé.
- NUMERICAL: ou son abréviation :NM: -> Réponse numérique 
- SHORTANSWER: ou son abréviation :SA: -> Réponse courte indifférente à la casse
- SHORTANSWER_C: ou son abréviation :SAC: -> Réponse courte sensible à la casse
</details>

3. Compléter les réponses possibles dans la fenêtre suivante : 
- Vous pouvez paramétrer la note par défaut.
- Entrez chaque réponse possible, le feedback et le score en pourcentage.

Vous pouvez ajouter une option de réponse en cliquant sur le bouton "+", supprimer des propositions de réponses en cliquant sur "X" et les réorganiser en utilisant les flèches. Finalisez votre saisie en cliquant sur le bouton **"Insérer"**. Le code correspondant apparaît alors dans votre texte.

![cloze](img/activite_test/qtype12.png#printscreen#centrer)

4. Renouvelez les opérations pour insérer d'autres propositions de réponses dans votre question.
5. Il est possible de compléter un **feedback général** : cette rétroaction s'affichera une fois les réponses de l'élève envoyées.
6. Facultatif : Ajouter un numéro d'identification pour retrouver facilement la question dans les catégories de la banque de questions. Il doit être unique dans chaque catégories de la banque de questions. Laissez vide la plupart du temps.
7. Vous pouvez également paramétrer des pénalités en cas de tentatives multiples.
8. Terminez votre question en cliquant sur "**Enregistrer**".


## Mots manquants

![texte à trous](img/activite_test/qtype4.png#printscreen#centrer)

La question "**Sélectionnez les mots manquants**" permet de créer un texte à compléter avec des menus déroulants pour faire des propositions.
1. Inscrivez, dans le champ Nom de question, le **nom de la question**. Il apparaîtra dans la banque de question.
2. Rédigez, dans le champ **Texte de la question**, l'énoncé de la question tel qu'il apparaîtra aux élèves.  
 Il faut respecter le format **[[1]]** pour les "trous"
3. Complétez les choix 
    - La bonne réponse 1 dans le choix n°1 : les autres propositions ensuite, organisées par groupes : par exemple, le groupe 1 sera composé du choix 1, et des autres choix possibles
    - Répéter l'opération en faisant attention de faire coïncider à chaque fois la bonne réponse avec le numéro entre crochets
4. Cliquez sur le bouton "**Enregistrer**" pour terminer.

## Glisser-déposer sur image

![glisser-déposer](img/activite_test/qtype5.png#printscreen#centrer)

La question "Glisser-déposer sur image" permet demander aux élèves de déposer des cases avec des propositions dans des cases vides sur une image.
1. Complétez le Nom de la question
2. Rédigez, à Texte de la question, l'énoncé de la question tel qu'il apparaîtra aux élèves.
3. Déposez une image de fond
(Les images trop grandes seront automatiquement redimensionnées) Votre image s'affiche dans l'Aperçu :

4. Ensuite, créez les éléments à glisser-déposer.
Ces éléments peuvent être sous forme de texte ou d'image.

:::tip **À savoir** :  
Validez votre saisie de texte avec la touche <code>Entrée</code> du clavier
:::

5. Dans la section **Zones de glisser-déposer** (ou Zones de dépôt), choisissez un élément pour la première réponse dans le menu déroulant Élément déplaçable.
 Lorsque cet élément est défini, il apparaît sous l'image de fond dans la section Aperçu.

6. **Déplacez les cases** ainsi créées au bon endroit sur l'image de fond.
Remarque : Dans la section Zones de dépôt, des coordonnées apparaissent dans les champs "Gauche" et "Haut"

7. **Enregistrez** votre question

## Stack

La question "**Stack**" permet de réaliser des évaluations formelles de Mathématiques où les valeurs d’énoncé sont randomisées. Les élèves auront ainsi des valeurs différentes. Stack repose sur un logiciel libre de calcul formel bien connu : Maxima (http://maxima.sourceforge.net/).

![stack](img/activite_test/qtype13.png#printscreen#centrer)

1. Dans le champ "**Nom de question**", indiquez le nom de la question. Cela vous permettra de la retrouver facilement dans la banque de questions.
2. Déclarez ensuite les variables utilisées pour la question. Ces variables peuvent être utilisées pour construire un énoncé randomisé ou pour évaluer la réponse de l’élève. Il est conseillé d’utiliser des commentaires pour structurer votre contenu. Les commentaires sont à placer entre les balises /* et */. A la fin de chaque déclaration utiliser un point-virgule.
3. Complétez l'énoncé de la question en utilisant des balises qui permettent de préciser la ou les variables utilisées pour la saisie de l'élève. Vous pouvez utiliser l'éditeur d'équation inclus dans l'éditeur de texte. Pour interpréter des commandes latex, il faut utiliser les balises `\`( et `\`).
Les valeurs des variables sont précédées de `{@` et suivies de `@}`.

Les zones de saisies des réponses ont la syntaxe suivante :
- [[input:ans1]] est le champ de saisie de la réponse de l’élève. [[validation:ans1]] permet de visualiser l’interprétation par STACK de la saisie (feedback de validation relatif à la syntaxe de la saisie de l'élève).
Ces deux champs sont présents par défaut dans le texte de la question.
- Il est possible d’insérer d’autres champs de réponses : [[input:ans2]] [[validation:ans2]]…
4. Indiquez la note par défaut (la note maximale pour cette question).
5. Il est possible de compléter un **feedback spécifique** pour chaque arbre de réponse potentiel :  Les arbres permettent d’afficher des feedbacks spécifiques suivant le parcours obtenu dans l’arbre.
Si vous avez, par exemple, deux zones de saisies. Il est conseillé d’avoir deux arbres indépendants pour analyser les réponses (lorsqu’il faut analyser plusieurs champs de réponse pour déterminer si la réponse de l’étudiant est correcte, il faut alors les regrouper dans un arbre unique). Il faudra alors déclarer le 2ème arbre :
[[feedback:prt1]]
[[feedback:prt2]]
6. Vous pouvez définir les pénalités en cas de mauvaise réponse.
7. Il est possible de compléter un **feedback général** : cette rétroaction s'affichera une fois les réponses de l'élève envoyées. Vous pouvez utiliser des commandes Latex ou l'éditeur d'équation pour faire apparaître les valeurs des variables obtenues pour l’élève.
8. Facultatif : Ajouter un numéro d'identification pour retrouver facilement la question dans les catégories de la banque de questions. Il doit être unique dans chaque catégories de la banque de questions. Laissez vide la plupart du temps.
9. Le champ "Question description" permet de décrire la question pour les autres enseignants : ce champ est invisible pour les élèves.
10. Le champ "Annotation de question" permet de distinguer les différentes versions des valeurs aléatoires obtenues. Cette fonctionnalité est utilisée pour établir des statistiques.
11. Cliquez sur le bouton "Vérifier le texte de la question et mettre à jour le formulaire" pour vérifier votre saisie avant de passer à la suite.

12. Complétez ensuite la rubrique **Input** :

Dans cette rubrique, on définit pour chaque zone de saisie la nature du type de réponse attendu, la nature de la réponse attendue (numérique, algébrique…), les mots interdits, les mots autorisés dans les champs de saisie …

13. Complétez enfin **l'arbre de réponse potentiel** :

On définit un arbre pour analyser la réponse de l’élève. Cet arbre est constitué d’un ou plusieurs nœuds.
- On commence par décrire la question.
- Dans le champ "Variables de feedback", il est possible de manipuler les valeurs saisies par l’étudiant. Comparer, prendre
le max. On peut aussi par exemple compter le nombre de bonnes ou mauvaises réponses, …
- On définie ensuite les noeuds : 

Un arbre est constitué d’un ou plusieurs nœuds.
Pour chaque noeud, on décrit comment on souhaite analyser la réponse de l'élève par rapport à la réponse attendue. On renseigne dans le champ "Test de réponse" le type de test à réaliser. 

Par exemple :

● **AlgEquiv** : vérifie si ans1 et correct1 sont algébriquement équivalents

● **FacForm** : vérifie si ans1 est sous forme factorisée

Le champ "**SAns**" (Student Answer) correspond à la réponse fournie par l’élève (telle qu’elle figure dans le texte de la question).

Le champ "**TAns**" (Teacher Answer) est la bonne réponse à la question. Ici l’expression algébrique g(x) peut être définie dans les variables de question.

Certains types de vérifications nécessitent des options.

Exemple d'arbre à 2 noeuds :

![stack](img/activite_test/qtype14.png#printscreen#centrer)

14. Finissez par **Enregistrer** votre question.


## Calculée

<iframe title="e-éducation &amp; Éléa - Intégrer des questions calculées dans une activité Test" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/6215a2d0-f6b2-4f3e-8e36-3cf35d976b13" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

