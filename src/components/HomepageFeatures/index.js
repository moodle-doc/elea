import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';
import PresentationSVG from '@site/static/img/team_up.svg';
import DocumentationSVG from '@site/static/img/completed_tasks.svg';
import EcosystemeSVG from '@site/static/img/community.svg';
import ConceptionsSVG from '@site/static/img/conceptions.svg';
import NomSVG from '@site/static/img/nom_elea.svg';

export default function HomepageFeatures() {
  return (
    <section className='features_src-components-HomepageFeatures-styles-module homepage'>
      <div className="container">
        <div className="row">
          <div className={clsx('col col--8')}>
          <NomSVG className="nomSVG"/>
            <div className={clsx('container padding-horiz--md')}>
            <div className={clsx('row tuile')}>
              <div className={clsx('col col--9')}>
                <h3 class="homepage">Un Moodle pour les enseignants et leurs élèves</h3>
                <p className={clsx('text--justify')}>Éléa est un commun numérique proposé par la DNE. C'est une plateforme numérique d'apprentissage adaptée à l'enseignement primaire et secondaire.</p>
          <p className={clsx('text--justify')}>Elle permet aux enseignants de concevoir aisément des parcours pédagogiques scénarisés, de les mettre en œuvre avec leurs élèves et de suivre leur progression.</p>
          <p className={clsx('text--justify')}>Elle propose différents types d'activités pour favoriser l'autonomie, la différenciation et la collaboration</p>
              </div>
              <div className={clsx('col col--3')}>
                <PresentationSVG className="homeSVG"/>
              </div>
          </div>          
            </div>
            <div className={clsx('row tuile')}>
            <div className={clsx('col col--3')}>
            <DocumentationSVG className="homeSVG"/>
              </div> 
              <div className={clsx('col col--9')}>
                <h3 class="homepage">La documentation</h3>
                <p className={clsx('text--justify')}>Cette documentation complète vous guide pas à pas dans l’utilisation de la plateforme. Découvrez comment concevoir un parcours et le mettre à disposition de vos élèves. Découvrez des usages de la plateforme présentés par des enseignants.</p>
              </div>         
            </div>
            <div className={clsx('row vignettes-container')}>
              <div class="lancement">
                <div class="card__body">
                    <ConceptionsSVG className="homeSVG"/>
                </div>
                <div class="card__footer">
                  <a href='docs/category/equipes'><button class="button button--primary button--block">Tutoriels</button></a>
                </div>
              </div>
              <div class="lancement">
                <div class="card__body">
                    <EcosystemeSVG className="homeSVG"/>
                </div>
                <div class="card__footer">
                <a href='docs/category/conceptions'><button class="button button--primary button--block">Usages</button></a>
                </div>
              </div>
              <div class="lancement">
                <div class="card__body">
                    <EcosystemeSVG className="homeSVG"/>
                </div>
                <div class="card__footer">
                <a href='docs/category/scenariser'><button class="button button--primary button--block">Scénarisation</button></a>
                </div>
              </div>   
            </div>           
            </div>
        <div className={clsx('marge col col--4')}>
          <div class='tuile'>
            <h4 class="homepage">Retrouver votre plateforme:</h4>
            <p ><img src="https://test-concepteurs.apps.education.fr/theme/image.php/rdc/theme/1731639620/login-school" class="p-1"/>
              <span class="p-1">Nom de l'établissement :</span>             </p>
            <p ><img src="https://test-concepteurs.apps.education.fr/theme/image.php/rdc/theme/1731639620/login-other" class="p-1"/>
              <span class="p-1">Commune de l'établissement :</span>...
            </p>
          </div>
        </div>
        </div>
      </div>

    </section>
  );
}
